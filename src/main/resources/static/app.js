var app = angular.module('aitu-project', [])

app.controller('RussCtrl', function($scope, $http){
    $scope.QuotesList = [];
    $scope.ParableList = [];

    $scope.getQuotes = function(){
        $http({
            url: 'http://127.0.0.1:8085/api/parable',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin":"*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response){
                    $scope.QuotesList = response.data;
                },
                function (response) {
                    console.log(response)
                });
    };


    $scope.getParable = function(){
        $http({
            url: 'http://127.0.0.1:8085/api/quotes',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin":"*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response){
                    $scope.ParableList = response.data;
                },
                function (response) {
                    console.log(response)
                });
    };

    $scope.getQuotes();
    $scope.getParable();
})
