package ProjectR.Group3.repository;

import ProjectR.Group3.model.Parable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParableRepository extends CrudRepository<Parable, Long> {
}
