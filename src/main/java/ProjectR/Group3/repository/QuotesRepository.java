package ProjectR.Group3.repository;


import ProjectR.Group3.model.Quotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuotesRepository extends CrudRepository<Quotes, Long> {
}
