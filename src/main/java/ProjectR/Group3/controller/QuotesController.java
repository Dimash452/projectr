package ProjectR.Group3.controller;

import ProjectR.Group3.service.QuotesService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class QuotesController {
    private final QuotesService quotesService;

    public QuotesController(QuotesService quotesService) {
        this.quotesService = quotesService;
    }

    @GetMapping("/api/quotes")
    public ResponseEntity<?> getQuotes(){
        return ResponseEntity.ok(quotesService.getAll());
    }
}
