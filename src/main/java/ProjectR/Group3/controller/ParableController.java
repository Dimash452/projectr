package ProjectR.Group3.controller;

import ProjectR.Group3.service.ParableService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ParableController {
    private final ParableService parableService;

    public ParableController(ParableService parableService) {
        this.parableService = parableService;
    }

    @GetMapping("/api/parable")
    public ResponseEntity<?> getParable(){
        return ResponseEntity.ok(parableService.getAll());
    }
}
