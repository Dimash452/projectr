package ProjectR.Group3.service;

import ProjectR.Group3.model.Parable;
import ProjectR.Group3.repository.ParableRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParableService {
    public final ParableRepository parableRepository;

    public ParableService(ParableRepository parableRepository) {
        this.parableRepository = parableRepository;
    }

    public List<Parable> getAll(){
        return (List<Parable>) parableRepository.findAll();
    }
}
