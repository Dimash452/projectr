package ProjectR.Group3.service;

import ProjectR.Group3.model.Quotes;
import ProjectR.Group3.repository.QuotesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuotesService {
    public final QuotesRepository quotesRepository;

    public QuotesService(QuotesRepository quotesRepository) {
        this.quotesRepository = quotesRepository;
    }

    public List<Quotes> getAll(){
        return (List<Quotes>) quotesRepository.findAll();
    }

}
